package be.intec.generate_pdf;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.io.StringReader;
import java.util.List;

import javax.xml.transform.Result;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.sax.SAXResult;
import javax.xml.transform.stream.StreamSource;

import org.apache.fop.apps.FOPException;
import org.apache.fop.apps.FOUserAgent;
import org.apache.fop.apps.Fop;
import org.apache.fop.apps.FopFactory;
import org.apache.fop.apps.MimeConstants;
import org.jdom.Document;
import org.jdom.Element;
import org.jdom.JDOMException;
import org.jdom.input.SAXBuilder;
import org.jdom.output.Format;
import org.jdom.output.XMLOutputter;
import org.xml.sax.SAXException;

/**
 * Transform XML file into multiple PDF's (JDom) 
 * Generates a separate pdf for every contact 
 */
public class SeparatePdfs
{
    public static void main( String[] args )
    {
    	// the XSL FO file
    	File xsltfile = new File("./src/main/resources/SeparateContacts.fo");
    	
    	//Get XML as list of JDOM elements 
        try {
            File inputFile = new File("./src/main/resources/Contacts.xml");
            SAXBuilder saxBuilder = new SAXBuilder();
            Document document = saxBuilder.build(inputFile);
            
            Element classElement = document.getRootElement();
            
            @SuppressWarnings("unchecked")
			List<Element> contactList = classElement.getChildren();
            
            //Render PDF for each contact 
            for (int temp = 0; temp < contactList.size(); temp++) { 
               Element contact = contactList.get(temp); 
               String fileName = contact.getChild("name").getText().replace(" ", "_").toLowerCase(); 
               //Get single contact as String 
               XMLOutputter outputter = new XMLOutputter(Format.getPrettyFormat());
               //Get string 
               String xmlString = outputter.outputString(contact); 
               System.out.println(xmlString);
               //Convert to streamSource 
               StreamSource source = new StreamSource(new StringReader(xmlString)); 
               // creation of transform source
	           StreamSource transformSource = new StreamSource(xsltfile);
	           // to store output
	           ByteArrayOutputStream outStream = new ByteArrayOutputStream();
	           
	           try
	           	{
	           		//Get transformer 
	        	   Transformer xslfoTransformer = getTransformer(transformSource);
	           		// Construct fop with desired output format
	           		FopFactory fopFactory = FopFactory.newInstance(xsltfile);
	       	    	// a user agent is needed for transformation
	           		FOUserAgent foUserAgent = fopFactory.newFOUserAgent();
	       	        Fop fop = fopFactory.newFop(MimeConstants.MIME_PDF, foUserAgent, outStream);
	       			// Resulting SAX events (the generated FO) 
	       			// must be piped through to FOP
	                Result res = new SAXResult(fop.getDefaultHandler());
	                
	       			// Start XSLT transformation and FOP processing
	       			try
	       			{
	       			    // everything will happen here..
	       				xslfoTransformer.transform(source, res);
	       				
	       				//Write to pdf 
	       				File pdffile = new File("target/Letter_" + fileName + ".pdf"); 
	       				OutputStream out = new java.io.FileOutputStream(pdffile);
	       	                        out = new java.io.BufferedOutputStream(out);
	       	                        FileOutputStream str = new FileOutputStream(pdffile);
	       	                        str.write(outStream.toByteArray());
	       	                        str.close();
	       	                        out.close();
	       	                        
	       			} catch (FileNotFoundException e) {
	       				e.printStackTrace();
	       			} catch (IOException e) {
	       				// TODO Auto-generated catch block
	       				e.printStackTrace();
	       			} catch (TransformerException e) {
	       				// TODO Auto-generated catch block
	       				e.printStackTrace();
	       			}
	       		} catch (FOPException e1) {
	       			e1.printStackTrace();
	       		} catch (SAXException e1) {
	       			// TODO Auto-generated catch block
	       			e1.printStackTrace();
	       		} catch (IOException e1) {
	       			// TODO Auto-generated catch block
	       			e1.printStackTrace();
	       		}
               
            }
         }catch(JDOMException e){
            e.printStackTrace();
         }catch(IOException ioe){
            ioe.printStackTrace();
		}
    	
    	// the XML file from which we take the name
    	//StreamSource source = new StreamSource(new File("./src/main/resources/Contacts.xml"));
    }
    /**
     * Get XSLT transformer 
     * @param streamSource
     * @return
     */
    private static Transformer getTransformer(StreamSource streamSource)
    {
    	// setup the xslt transformer
    	net.sf.saxon.TransformerFactoryImpl impl = 
    			new net.sf.saxon.TransformerFactoryImpl();

    	try {
    		return impl.newTransformer(streamSource);

    	} catch (TransformerConfigurationException e) {
    		e.printStackTrace();
    	}
    	return null;
    }    
}
