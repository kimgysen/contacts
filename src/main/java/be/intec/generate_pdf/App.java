package be.intec.generate_pdf;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;

import javax.xml.transform.Result;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.sax.SAXResult;
import javax.xml.transform.stream.StreamSource;

import org.apache.fop.apps.FOPException;
import org.apache.fop.apps.FOUserAgent;
import org.apache.fop.apps.Fop;
import org.apache.fop.apps.FopFactory;
import org.apache.fop.apps.MimeConstants;
import org.xml.sax.SAXException;

/**
 * Transform XML with contacts file into PDF
 * Generates 1 pdf for all contacts 
 *
 */
public class App 
{
    public static void main( String[] args )
    {
    	// the XSL FO file
    	File xsltfile = new File("./src/main/resources/Contacts.fo");
    	// the XML file from which we take the name
    	StreamSource source = new StreamSource(new File("./src/main/resources/Contacts.xml"));
    	// creation of transform source
    	StreamSource transformSource = new StreamSource(xsltfile);
    	// to store output
    	ByteArrayOutputStream outStream = new ByteArrayOutputStream();
    	
    	try
    	{
    		Transformer xslfoTransformer = getTransformer(transformSource);
    		// Construct fop with desired output format
    		FopFactory fopFactory = FopFactory.newInstance(xsltfile);
	    	// a user agent is needed for transformation
    		FOUserAgent foUserAgent = fopFactory.newFOUserAgent();
	        Fop fop = fopFactory.newFop(MimeConstants.MIME_PDF, foUserAgent, outStream);
			// Resulting SAX events (the generated FO) 
			// must be piped through to FOP
            Result res = new SAXResult(fop.getDefaultHandler());
            
			// Start XSLT transformation and FOP processing
			try
			{
			    // everything will happen here..
				xslfoTransformer.transform(source, res);
				// if you want to get the PDF bytes, use the following code
				//return outStream.toByteArray();
				
				//Write to pdf 
				File pdffile = new File("target/Contacts.pdf"); 
				OutputStream out = new java.io.FileOutputStream(pdffile);
	                        out = new java.io.BufferedOutputStream(out);
	                        FileOutputStream str = new FileOutputStream(pdffile);
	                        str.write(outStream.toByteArray());
	                        str.close();
	                        out.close();
	                        
			} catch (FileNotFoundException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			} catch (TransformerException e) {
				e.printStackTrace();
			}
		} catch (FOPException e1) {
			e1.printStackTrace();
		} catch (SAXException e1) {
			e1.printStackTrace();
		} catch (IOException e1) {
			e1.printStackTrace();
		}
    }
    /**
     * Get XSLT transformer 
     * @param streamSource
     * @return
     */
    private static Transformer getTransformer(StreamSource streamSource)
    {
    	// setup the xslt transformer
    	net.sf.saxon.TransformerFactoryImpl impl = 
    			new net.sf.saxon.TransformerFactoryImpl();

    	try {
    		return impl.newTransformer(streamSource);

    	} catch (TransformerConfigurationException e) {
    		e.printStackTrace();
    	}
    	return null;
    }    
}
